using System;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VRPSolver.Test
{
    [TestClass]
    public class VehicleTest
    {
        [TestMethod]
        public void TestAddingNewLocations()
        {
            var locationMock = new Mock<Location>();
            //locationMock
            //    .Setup(loc => loc.Id).Returns(1);
                locationMock
                .Setup(loc => loc.GetNeighboursWithinRadius(2)).Returns(
                new System.Collections.Generic.List<long>() { 2, 3 });
            var location = locationMock.Object;
            Assert.AreEqual(2, location.GetNeighboursWithinRadius(2)[0]);
        }
    }
}
