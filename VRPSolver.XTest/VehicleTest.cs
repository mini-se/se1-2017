using System;
using Moq;
using Xunit;

namespace VRPSolver.Test
{
    public class VehicleTest
    {
        [Fact]
        public void TestAddingNewLocations()
        {
            var locationMock = new Mock<Location>();
            //locationMock
            //    .Setup(loc => loc.Id).Returns(1);
            locationMock
            .Setup(loc => loc.GetNeighboursWithinRadius(2)).Returns(
            new System.Collections.Generic.List<long>() { 2, 3 });
            var location = locationMock.Object;
            Assert.Equal(2, location.GetNeighboursWithinRadius(2)[0]);
        }
    }
}
