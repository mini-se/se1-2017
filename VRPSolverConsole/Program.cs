﻿using CommandLine;
using log4net;
using log4net.Config;
using log4net.Repository;
using log4net.Core;
using System;
using System.Reflection;
using System.IO;

namespace VRPSolverConsole
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static Program()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }
        class Options
        {
            [Option('d', "dummy", Required = true,
              HelpText = "dummy required integer parameter")]
            public int Dummy { get; set; }

            [Option('f', "foo", Required = false,
                Default = 0.01,
                HelpText = "dummy optional double parameter")]
            public double Foo { get; set; }

        }
        static void Main(string[] args)
        {


            var parsed = CommandLine.Parser.Default.ParseArguments<Options>(args);
            if (parsed.Tag == ParserResultType.NotParsed)
            {
                return;
            }
            Options options = (Options)parsed.MapResult(
                (Options opt) => opt,
                _ =>
                {
                    throw new NotImplementedException();
                });

            log.Info($"Dummy set to {options.Dummy}");
            log.Info($"Foo set to {options.Foo}");
        }
    }

}