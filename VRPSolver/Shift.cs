﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VRPSolver
{
    /// <summary>
    /// Shift controls the time constrains
    /// </summary>
    public class Shift
    {
        List<Route> routes;

        public double MaxTotalTime { get; private set; }

        public bool CanAdd(Location location)
        {
            var totalTime = routes.Sum(rt => rt.TotalTime);
            var timeIncreaseConstraint = Math.Max(0, MaxTotalTime - totalTime);
            return routes.Any(rt => rt.CanAdd(location, timeIncreaseConstraint));
        }
    }
}
