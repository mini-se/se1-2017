﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VRPSolver
{
    public interface ILocationLocated
    {
        long Id { get; }
        double X { get; }
        double Y { get; }
    }
}
