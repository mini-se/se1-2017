﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VRPSolver
{
    public interface ILocationMass
    {
        long Id { get; }
        double Mass { get; }
    }
}
