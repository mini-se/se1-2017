﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VRPSolver
{
    /// <summary>
    /// Vehicle controls the shifts
    /// </summary>
    public class Vehicle
    {
        List<Shift> shifts;

        public bool CanAdd(Location location)
        {
            return shifts.Any(shft => shft.CanAdd(location));
        }
    }
}
